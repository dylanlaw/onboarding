# Common Environment - Start Here  

## Sections

- [Introduction](#introduction)
  - [New Member Orientation](#new-member-orientation)
  - [Our Goals, Purpose and Values](#our-goals-purpose-and-values)
  - [Our Development Workflow](#our-development-workflow)
  - [Platform Roadmap](#platform-roadmap)
- [Teams and Leads](#teams-and-leads)
- [Required Software](#required-software)
- [Required Accounts](#required-accounts)
- [Other Resources Needed](#other-resources-needed)

## Introduction

### New Member Orientation

Please listen to the [New Member Orientation][orientation_audio_url] (includes a transcript) so you can be up to speed on the overall mission of MFD. The duration is approximately one hour.

### Our Goals, Purpose and Values

The goal of the logistics platform is to provide each local site around the world with:

- a public-facing web presence (in the local language) that identifies who the site's key people are, how to get involved, where to donate (locally), and what top needs are
- "how to get involved" here ranges from putting people into the main M4D volunteer funnel to where to drop off supplies in your neighborhood
- a single place to track and quantify their material and volunteer resources
- a single place to track and quantify the PPE needs of their community
- an automated means of aggregating supply and demand at the local grassroots level to facilitate collection and distribution both locally and across regions
- a means of bookkeeping quality verification for community-sourced PPE

Anti-goals are building a customer relationship management system (CRM), a messaging platform (slack and email work great!), an org social network, or an international shipping management system: there are great tools for those things already.

When it comes to other orgs, our tools should act as the interface between their process and the chaos of large scale grassroots organizing. Let's be sure to include them in the conversation!

There are great opportunities for this system to fit into what other orgs can help us with: we can integrate with The Supply Chain Federation's project so they can help identify trusted industrial sources of PPE while we feed back to them trusted community distributors of PPE. A CMS will be helpful for tracking points of contact and communications.

Some values our team is holding in building out platform are for us to be:

- **Open** - We want to share our experience and the result of our efforts as widely as possible. Open source code, public informational resources, translation & internationalization, etc.

- **Healthy** - We're all going all out, but need to take care of ourselves to maintain this effort long term. It's ok to take break, or log off for the night. Expect that folks may not get back to you right away.

- **Inclusive** - Anybody can contribute to fighting COVID. And everybody needs to! Our team should be a safe and respectful place for all sorts of folks. And you never know what someone's day has been like: it's ok if we're a bit emotional from time to time. Diversity, accessibility, #LeaveNoOneBehind.

- **Grassroots** - Our priority is supporting grassroots organizing. We will be interacting with a lot of other orgs that have different priorities, and need to consistently choose to do the best thing for our local sites. Taylor F. had an amazing call advising a new site lead in Bogata today. Our team aims to be close contact with the people relying on us.

Above all, our team is highly focused on the logistical needs of our local site leads in their task of organizing in their communities.

-- by `Bion Johnson - Seattle`  |  2020-03-27

### Our Development Workflow

Please review our Development Workflow [here][dev_workflow_url]. It details our guidelines for making our work visible and avoiding working at cross-purposes.

### Platform Roadmap

The Platform Roadmap is located [here][roadmap_url]. It lays out an approach to moving Masks For Docs (MDF) from its current Typeforms + Google Sheets process to one that is more robust, scalable, and efficient.

## Teams and Leads

1. The Technical Lead is **Bion Johnson** `Bion Johnson - Seattle` (Seattle GMT-7)

1. The Front End lead is **David Golightly** `David Golightly (he/him) - SFBay` (Berkeley GMT-7).

   Most tasks will be around building a browser client using React, Redux, SASS, and GraphQL. In addition to those technologies there's a good opportunity for displaying a map.

1. The Back End lead is **Taylor Fairbank** `Taylor (he/him)` (Belgrade GMT+1).

   There is already a team and platform for the back end, but that team could use more help as well. The back end is written in Elixir using the Phoenix framework backed by a Postgres DB.

1. The DevOps lead is **Ariana Bray** `Ariana Bray (she/her)` (Seattle GMT-7).

    We're going to be running a production system that orchestrates a pipeline of protective equipment to front-line healthcare workers. We need dev ops folks to help us stand up and maintain staging and production environments, configure CI, standardize development workflows, and instrument monitoring and alerting.

## Required Software

### Source Code Management

The application repository is located [here][toolbox_repo_url].  Includes directions for getting up and running.

### docker

### docker-compose

## Required Accounts

## Other Resources Needed

## Next Steps

Please return to the [README.md](./README.md) and if necessary visit the onboarding guide that is relevant to your role.

## Document Maintainer

The maintainer of this document is **Pete Nuwayser** `pete nuwayser (DC) he/his` (GMT-4). Pete can help get you started and pointed in the right direction.

[dev_workflow_url]: https://www.notion.so/Development-workflow-63d2215fe9704bd5819563c573941f98
[roadmap_url]: https://docs.google.com/document/d/1EH7ygTBbkb7_HzPqBetiEDXaxEAkzbpTh997ICGZw5M/mobilebasic#
[orientation_audio_url]: https://habitu8.zoom.us/rec/play/vpd7Je36qmk3SYXA5gSDC_MrW9S-LqKs23RP-6UFyUjhVyQHMVOgM-caNuNLnzsspqH9Jaw88UjRpuvK?continueMode=true&_x_zm_rtaid=WxRlWsdbTuisRq444J193g.1585095523755.870301edf2a1b3cca6ea2704d6a17633&_x_zm_rhtaid=513
[toolbox_repo_url]: https://gitlab.com/masksfordocs/toolbox
