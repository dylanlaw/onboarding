# DevOps - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for new CI/CD team members. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to help automate the development, testing and deployment processes. Bring work to the people (not people to the work)!

## Teammates Who Can Help

Our fearless leader is Ariana Bray (Seattle GMT-7)

We are organized by skill area and try to have at least two team members covering every skill. Please check out our [Open Needs](./OpenNeeds.md) for what we need help with.

- **Cloud & Terraform:** `Cory Parent` & `Tehut (she/her) - Seattle`
- **Containerization:** `Ariana Bray (she/her)` & `Cory Parent`
- **Pipeline:** `Ariana Bray (she/her)` & `pete nuwayser (DC) he/his`
- **Observability/Monitoring:** `Cory Parent` & `Tehut (she/her) - Seattle`
- **Security:** `peterloren` & `Kristen (she/her)`
- **Performance:** OPEN & OPEN
- **New Dev Onboarding (incl this guide):** `pete nuwayser (DC) he/his` & `Eric H (he/his)`
- **AWS Infrastructure** `peterloron` & `Cory Parent` & `Tehut (she/her) - Seattle`

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role.

1. Placeholder for the pipeline stack component documentation.

> **TO-DO** add prerequisites for FE and BE that didn't belong in Common.

## Required Accounts

> **TO-DO** this should be for any accounts requiring manual setup, or other things that may need to be approved, that are not included in Common. The hope is to automate account creation as much as possible.

## Required Software

### AWS CLI

### Terraform

The Terraform repository is located [here][terraform_repo_url]. You will need to request AWS credentials from Ari Bray in the #dev-ops channel.

### Packer

The initial MVP is using a very simple on-host Docker deployment. The Packer code for constructing the AMI can is located [here][packer_repo_url]

## Other Resources Needed

[packer_repo_url]: https://gitlab.com/masksfordocs/platform-ami
[terraform_repo_url]: https://gitlab.com/masksfordocs/aws-infra
