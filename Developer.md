# Developer - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for front-end and back-end developers. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to float between FE and BE work as needed. Bring work to the people (not people to the work)!

- The supply chain tools repository is located [here][toolbos_repo_url].

> **TO-DO** Update the intro

## Teammates Who Can Help

1. The maintainer of this document is [Pete Nuwayser][pri_maintainer_email] (DC GMT-4). Pete can help get you started and pointed in the right direction.

1. The Front End lead is [David Golightly][fe_lead_email] (Berkeley GMT-7).

1. The Back End lead is [Taylor Fairbank][be_lead_email] (Belgrade GMT+1).

1. Other team members: Bion Johnson, Jeff Khan

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role

  1. Clone the toolbox repository and follow the steps in the README.

  1. Review the backlog in the [Issues Board][issues_board_url].

> **TO-DO** add prerequisites for FE and BE that didn't belong in Common.

## Required Accounts

## Required Software

## Other Resources Needed

[pri_maintainer_email]: mailto:nuwayser@gmail.com?subject=mfdog-q-Developer
[toolbox_repo_url]: https://gitlab.com/masksfordocs/toolbox
[issues_board_url]: https://gitlab.com/masksfordocs/toolbox/-/boards
